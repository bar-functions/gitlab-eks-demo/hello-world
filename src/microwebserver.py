# From https://gist.github.com/davidbgk/b10113c3779b8388e96e6d0c44e03a74
import http.server
import socketserver
import os
from http import HTTPStatus

class Handler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        username = os.environ.get('username')
        password = os.environ.get('password')

        htmlStr = f'''<HTML>
        <BODY style="background:lightsalmon">
        Hello world from DEFAULT superlight Python web server.
        <table>
        <tr><td>Username</td><td>Password</td></tr>
        <tr><td>{username}</td><td>{password}</td></tr>
        </table>
        </BODY></HTML>'''

        self.send_response(HTTPStatus.OK)
        self.end_headers()
        self.wfile.write(bytes(htmlStr, 'utf-8'))

httpd = socketserver.TCPServer(('', 5000), Handler)
httpd.serve_forever()
